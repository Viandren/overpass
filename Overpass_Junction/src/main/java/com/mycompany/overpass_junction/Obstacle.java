/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.overpass_junction;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JComponent;
import javax.swing.JPanel;

/**
 *
 * @author csordasadam
 */
class Obstacle extends JPanel {

    Lane parentLane;
    int width;
    Image imageCar;

    public Obstacle(Lane parentLane, int dir) {
        this.parentLane = parentLane;
        Props p = new Props();
        int r = (int) (Math.random() * p.getGoingLeft().size());
        if (dir == 1) {
            imageCar = p.getGoingRight().get(r);
        } else if (dir == -1) {
            imageCar = p.getGoingLeft().get(r);
        }
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(imageCar, 0, 0, null);
    }

}
