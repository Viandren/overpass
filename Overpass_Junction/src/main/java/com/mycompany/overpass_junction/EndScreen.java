/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.overpass_junction;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;

/**
 *
 * @author robb
 */
public class EndScreen extends JDialog {

    
    
    
    public EndScreen(Game g) {
        
        setTitle("Game Over");
        setResizable(false);
        setLayout(null);
        setBounds(350, 200, 400, 200);
        //this.setDefaultCloseOperation(onclose());
        JButton newGame = new JButton("New Game");
        JButton exit = new JButton("Exit");
        add(newGame);
        add(exit);
        newGame.setBounds(60, 75, 110, 50);
        exit.setBounds(230, 75, 110, 50);
        setVisible(false);
        
        exit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onclose();
            }
        });
        
        newGame.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                g.p.score=0;
                g.level = 0;
                g.resetGame();
                setVisible(false);
            }
        });
    }
    
    private int onclose(){
        System.exit(0);
        return 0;
    }

}
