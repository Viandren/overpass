/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.overpass_junction;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;

/**
 *
 * @author robb
 */
public class TopScores {
    public static boolean running = false;
    HashMap<String,Integer> scores;
    int actualScore;
    String path = "TopScores.txt";
    ArrayList<HashMap.Entry<String,Integer>> orderedScores;

    public TopScores( int sc) {
        running = true;
        actualScore = sc;
        this.scores = new HashMap<>();
        readFromFile();
        orderedScores = orderList();
        
        for (java.util.Map.Entry<String, Integer> orderedScore : orderedScores) {
            System.out.println(orderedScore.getValue());
        }
        
        if (actualScore > orderedScores.get(orderedScores.size()-1).getValue()){
            saveScore();
        }
        
    }
    
    public void readFromFile(){
        
        for (String line : readLines(path)) {
            String[] data = line.split(Pattern.quote("|"));
            scores.putIfAbsent(data[0], Integer.parseInt(data[1]));
        }
        
    }
    
    
    private static ArrayList<String> readLines(String path) {
        ArrayList<String> res = new ArrayList<>();
        try (BufferedReader bf = new BufferedReader(new FileReader(path))) {

            String line = bf.readLine();

            while (line != null) {
                res.add(line);
                line = bf.readLine();
            }
        } catch (IOException e) {

        }
        return res;
    }
    
    public ArrayList<HashMap.Entry<String,Integer>> orderList(){
        ArrayList<HashMap.Entry<String,Integer>> lst = new ArrayList<>();
        lst.addAll(scores.entrySet());
        Collections.sort(lst, new Comparator<HashMap.Entry<String, Integer>>() {
            @Override
            public int compare(HashMap.Entry<String,Integer> o1, HashMap.Entry<String,Integer> o2) {
                return o2.getValue() - o1.getValue();
               }
        });
        return lst;
    }
    
    public void saveScore(){
        JOptionPane op = new JOptionPane();
        String name = op.showInputDialog("Enter your name");
        
        scores.putIfAbsent(name, actualScore);
        orderedScores = orderList();
        List<String> lines = new ArrayList<>();
        for (int i = 0; i < 10 && i < orderedScores.size(); i++) {
            lines.add(orderedScores.get(i).getKey()+"|"+orderedScores.get(i).getValue());
        }
        
        
        
        try {
        
        Path file = Paths.get(path);
        
        Files.write(file, lines, Charset.forName("UTF-8"));

        } catch (IOException e){
            
        }
        running = false;
    }
    
}
