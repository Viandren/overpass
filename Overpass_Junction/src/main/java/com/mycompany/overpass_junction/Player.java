/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.overpass_junction;

import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JComponent;

/**
 *
 * @author csordasadam
 */
public class Player extends JComponent {

    private final static int SIZE = 44;
    Point center;
    int score;

    public void move() {

    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        try {
            Image playerTexture = ImageIO.read(new File("src/textures/Ball_V3.png"));
            g.drawImage(playerTexture, 0, 0, this);

        } catch (IOException ex) {
            Logger.getLogger(Player.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public int getSIZE() {
        return SIZE;
    }

    public int getScore() {
        return score;
    }
    
    public void setScore(){
        score++;
    }

    public boolean hasCrashed(Obstacle obs) {
        center = new Point(getX() + 21, getY() + 21);
        Point a = new Point(obs.getX(), obs.getParent().getY() + obs.getY()); //bal felső sarok
        Point b = new Point(obs.getX() + 80, a.getY()); //jobb felső
        Point c = new Point(obs.getX(), a.getY() + 46); //bal alsó
        Point d = new Point(obs.getX() + 80, a.getY() + 46);  //jobb alsó

        if (center.getDistanceFrom(a) <= SIZE / 2) {
            return true;
        }
        if (center.getDistanceFrom(b) <= SIZE / 2) {
            return true;
        }
        if (center.getDistanceFrom(c) <= SIZE / 2) {
            return true;
        }
        if (center.getDistanceFrom(d) <= SIZE / 2) {
            return true;
        }

        return false;
    }

}
