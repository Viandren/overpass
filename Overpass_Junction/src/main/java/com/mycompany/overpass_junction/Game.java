/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.overpass_junction;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

/**
 *
 * @author robb
 */
public class Game extends JFrame implements KeyListener {

    Map m;
    Welcome w;
    Player p;
    int pos = 0;
    int maxPos = 0;
    public static boolean gameOver = false;
    EndScreen endScreen;
    JLayeredPane pane;
    ScoreBoard sb;
    JLabel score;
    JLabel grat = new JLabel("   Level Up!");
    int level = 0;
    
    boolean keyDown = false;

    public Game() {
        run();
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                Game g = new Game();
            }
        });

    }

    public void run() {
        setResizable(false);
        setSize(800, 600);
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        w = new Welcome(this);
        add(w);

        sb = new ScoreBoard(this);

        setVisible(true);

    }

    public void startNewGame() {
        if (w != null) {
            //remove(w);
            //repaint();
            w.setVisible(false);
        }
        AudioInputStream audioIn;
        try {
            audioIn = AudioSystem.getAudioInputStream(new File("src/audio/highway.wav"));
            Clip clip = AudioSystem.getClip();
            clip.open(audioIn);
            clip.start();
        } catch (UnsupportedAudioFileException ex) {
            Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
        } catch (LineUnavailableException ex) {
            Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
        }

        pane = new JLayeredPane();
        pane.setBounds(0, 0, 800, 600);
        p = new Player();
        p.setBounds(378, 482, p.getSIZE(), p.getSIZE());
        m = new Map(p, level);
        pane.add(m, 1);

        pane.add(p, 0);
        score = new JLabel("Your score: " + p.getScore());
        score.setBounds(25, 25, 200, 50);
        score.setForeground(Color.ORANGE);
        pane.add(score, 0);

        add(pane);

        addKeyListener(this);

        waitingForCrash();

        endScreen = new EndScreen(this);
        requestFocus();
    }

    public void showHighScores() {
        w.setVisible(false);
        add(sb);
    }

    public void movePlayer(int dir) {
        //dir: -1 -> lefelé
        //dir:  1 -> felfelé
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                if (dir == 1 && pos < 8) {
                    Timer t = new Timer(3, new ActionListener() {
                        int cnt = 0;

                        @Override
                        public void actionPerformed(ActionEvent evt) {
                            p.setBounds(p.getX(), p.getY() - 1, p.getSIZE(), p.getSIZE());
                            cnt++;
                            if (cnt == 55 || gameOver) {
                                if (pos > maxPos) {
                                    maxPos = pos;
                                    p.setScore();
                                    score.setText("Your score: " + p.getScore());
                                    if (pos == 8) {
                                        score.requestFocus();
                                        m.stopTimer = true;
                                        resetGame();
                                    }
                                }
                                keyDown = false;
                                ((Timer) evt.getSource()).stop();
                            }
                        }
                    });
                    t.start();
                    pos++;
                } else if (dir == -1 && pos > 0) {
                    Timer t = new Timer(3, new ActionListener() {
                        int cnt = 0;

                        @Override
                        public void actionPerformed(ActionEvent evt) {
                            p.setBounds(p.getX(), p.getY() + 1, p.getSIZE(), p.getSIZE());
                            cnt++;
                            if (cnt == 55 || gameOver) {
                                keyDown = false;
                                ((Timer) evt.getSource()).stop();
                            }
                        }
                    });
                    t.start();
                    pos--;
                }
            }
        });
    }

    public void waitingForCrash() {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {

                Timer t = new Timer(3, new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent evt) {

                        for (Lane lane : m.getLanes()) {
                            if (!lane.getObstacles().isEmpty()) {
                                for (Obstacle obs : lane.getObstacles()) {
                                    if (p.hasCrashed(obs)) {
                                        gameOver = true;
                                        endScreen.setVisible(true);
                                        if(!TopScores.running){
                                        TopScores ts = new TopScores(p.getScore());
                                        }
                                        ((Timer) evt.getSource()).stop();
                                    }
                                }
                            }
                        }

                    }
                });
                t.start();

            }
        });
    }


    public void resetGame() {
        pos = 0;
        maxPos = 0;
        gameOver = false;
        for (Lane lane : m.getLanes()) {
            lane.removeAll();
        }
        pane.removeAll();
        pane.repaint();

        p.setBounds(378, 482, p.getSIZE(), p.getSIZE());
        level++;
        m = new Map(p, level);
        pane.add(m, 1);

        pane.add(p, 0);
        score = new JLabel("Your score: " + p.getScore());
        score.setBounds(250, 25, 200, 50);
        score.setForeground(Color.ORANGE);
        pane.add(score, 0);

        if (p.getScore()!=0){
        grat.setBounds(10, 10, 100, 50);
        Border bord = new LineBorder(Color.RED, 4);
        grat.setForeground(Color.MAGENTA);
        grat.setBorder(bord);
        pane.add(grat, 0);

        Thread th = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException ex) {
                }
                grat.setVisible(false);
            }
        });
        th.start();
        }
        waitingForCrash();

        requestFocus();
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (!gameOver && !keyDown) {
            keyDown = true;
            int keyCode = e.getKeyCode();
            switch (keyCode) {
                case KeyEvent.VK_UP:
                    movePlayer(1);

                    break;
                case KeyEvent.VK_DOWN:
                    movePlayer(-1);

            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

}
