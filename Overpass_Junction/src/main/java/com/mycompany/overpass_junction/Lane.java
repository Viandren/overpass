/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.overpass_junction;

import java.awt.Color;
import java.util.ArrayList;
import javax.swing.JPanel;

/**
 *
 * @author csordasadam
 */
public class Lane extends JPanel {

    int speed;
    Color color;
    boolean isActive;
    int height;
    ArrayList<Obstacle> obstacles;
    int direction;
    int level;

    public Lane(int height, Color color, int direction, int level) {
        this.level = level;
        speed = (int) (Math.random() * (level+2) +2) * direction;
        setLayout(null);
        this.height = height;
        this.color = color;
        setBackground(color);
        isActive = true;
        obstacles = new ArrayList<>();
        this.direction = direction;
        addCars();
    }

    public Lane(int height, Color color) {
        setLayout(null);
        this.height = height;
        this.color = color;
        setBackground(color);
        this.isActive = false;
        obstacles = new ArrayList<>();

    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public boolean isIsActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public synchronized void moveCars() {

        for (Obstacle obstacle : obstacles) {
            if (obstacle != null && obstacle.getRootPane() != null) {
                int r = (int) (Math.random() * 5);
                obstacle.setBounds(obstacle.getX() + speed, obstacle.getY(), 80, 46);
                if (obstacle.getX() > obstacle.getRootPane().getWidth()) {
                    obstacle.setBounds(-80 - r, obstacle.getY(), 80, 46);
                }
                if (obstacle.getX() < -obstacle.getWidth()) {
                    obstacle.setBounds(obstacle.getRootPane().getWidth() + r, obstacle.getY(), 80, 46);
                }
            }
        }

    }

    public void addCars() {

        if (isActive) {
            int r = Math.min((int) (Math.random() * level + 2),(int) (Math.random() * 3 + 2));
            for (int i = 0; i < r; i++) {
                Obstacle car = new Obstacle(this, direction);
                car.setBounds(i * 200 + (int) (Math.random() * 100) + getX(), getY() + 2, 80, 46);
                add(car);
                obstacles.add(car);
            }
        }

    }

    public int getDirection() {
        return direction;
    }

    public ArrayList<Obstacle> getObstacles() {
        return obstacles;
    }

}
