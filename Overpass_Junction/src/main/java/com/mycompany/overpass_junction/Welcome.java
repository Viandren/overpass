/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.overpass_junction;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.Timer;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

/**
 *
 * @author herminades
 */
public class Welcome extends JPanel {

    private Game game;

    public Welcome(Game game) {
        this.game = game;
        init();
    }

    private void init() {

        //main window
        setSize(800, 600);

        BorderLayout borderLayout = new BorderLayout();
        setLayout(borderLayout);
        setBackground(Color.MAGENTA);

        //header
        JLabel header = new JLabel("OVERPASS JUNCTION", SwingConstants.CENTER);
        Font headerFont = new Font(Font.MONOSPACED, Font.PLAIN, 60);
        header.setForeground(Color.black);
        header.setFont(headerFont);
        Border headerBorder = new LineBorder(Color.yellow, 8, true);
        header.setBorder(headerBorder);
        add(header, BorderLayout.NORTH);

        //footer
        JLabel footer = new JLabel("<HTML> Cross the roads and obstacles as far as possible without dying. The player must use the arrows on the keyboard in the appropriate direction to move the character horizontally. Every forward movement will earn one point. </HTML>");
        Font footerFont = new Font(Font.MONOSPACED, Font.PLAIN, 12);
        footer.setFont(footerFont);
        Border footerBorder = new LineBorder(Color.YELLOW, 4);
        footer.setBorder(footerBorder);
        add(footer, BorderLayout.SOUTH);

        //center panel
        JPanel center = new JPanel();
        BoxLayout boxLayout = new BoxLayout(center, BoxLayout.X_AXIS);
        center.setLayout(null);
        center.setBackground(Color.magenta);

        //center //start button
        JButton startGameButton = new JButton("Start Game");
        Font buttonFont = new Font(Font.MONOSPACED, Font.PLAIN, 26);
        startGameButton.setFont(buttonFont);
        startGameButton.setBounds(270, 140, 225, 75);

        //TODO button broder, background nem működik
        //Border buttonBorder = new LineBorder(Color.cyan, 4);
        //startGameButton.setBorder(buttonBorder);
        //actionlistener
        startGameButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                game.startNewGame();

            }
        });
        center.add(startGameButton);
        add(center);

        //moving cars design up
        Lane lane = new Lane(50, new Color(105, 105, 105));
        lane.setBounds(0, 60, 800, lane.getHeight());

        Obstacle o = new Obstacle(lane, -1);
        o.setBounds(200, 60, 80, 46);
        center.add(o);
        center.add(lane);

        Timer t = new Timer(10, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                o.setBounds(o.getX() - 2, o.getY(), 80, 46);

                if (o.getX() < -80) {
                    o.setBounds(o.getRootPane().getSize().width, o.getY(), 80, 46);
                }
                Toolkit.getDefaultToolkit().sync();
            }
        });
        t.start();

        //moving cars design down
        Lane lane2 = new Lane(50, new Color(105, 105, 105));
        lane2.setBounds(0, 330, 800, lane2.getHeight());

        Obstacle o2 = new Obstacle(lane2, 1);
        o2.setBounds(200, 330, 80, 46);
        center.add(o2);
        center.add(lane2);

        Timer t2 = new Timer(10, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                o2.setBounds(o2.getX() + 2, o2.getY(), 80, 46);

                if (o2.getX() > 800) {
                    o2.setBounds(-80, o2.getY(), 80, 46);
                }
                Toolkit.getDefaultToolkit().sync();
            }
        });
        t2.start();
        
        
        
        JButton scoreButton = new JButton("Leaderboard");
        
        scoreButton.setFont(buttonFont);
        scoreButton.setBounds(270, 225, 225, 75);

        //actionlistener
        scoreButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                game.showHighScores();

            }
        });
        center.add(scoreButton);

        setVisible(true);
        
        
        
        
    }

}
