/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.overpass_junction;

import java.awt.Color;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.Timer;

/**
 *
 * @author csordasadam
 */
public class Map extends JPanel {

    ArrayList<Lane> lanes;
    final static Color GRASSCOLOR = new Color(101, 169, 57);
    final static Color GRAYCOLOR = new Color(105, 105, 105);
    final static Color SEPARATOR = new Color(255, 255, 159);
    final static Color SEPARATOR2 = new Color(75, 98, 135);
    Player p;
    
    boolean stopTimer = false;

    public Map(Player p, int level) {
        this.p = p;
        setLayout(null);
        setSize(800, 600);
        lanes = new ArrayList<>();
        lanes.add(new Lane(95, GRASSCOLOR));
        lanes.add(new Lane(50, GRAYCOLOR, -1,level));
        lanes.add(new Lane(5, SEPARATOR));
        lanes.add(new Lane(50, GRAYCOLOR, -1,level));
        lanes.add(new Lane(5, SEPARATOR));
        lanes.add(new Lane(50, GRAYCOLOR, -1,level));
        lanes.add(new Lane(5, SEPARATOR2));
        lanes.add(new Lane(50, GRASSCOLOR));
        lanes.add(new Lane(5, SEPARATOR2));
        lanes.add(new Lane(50, GRAYCOLOR, 1,level));
        lanes.add(new Lane(5, SEPARATOR));
        lanes.add(new Lane(50, GRAYCOLOR, 1,level));
        lanes.add(new Lane(5, SEPARATOR));
        lanes.add(new Lane(50, GRAYCOLOR, 1,level));
        lanes.add(new Lane(100, GRASSCOLOR));

        int x = 0;
        int y = 0;
        for (Lane lane : lanes) {

            lane.setBounds(x, y, 800, lane.getHeight());
            y = y + lane.getHeight();
            add(lane);

        }

        play();
    }

    public void play() {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                Timer t = new Timer(20, new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent evt) {
                        if (Game.gameOver || stopTimer) {
                                ((Timer) evt.getSource()).stop();
                            }
                        for (Lane lane : lanes) {
                            lane.moveCars();
                        }
                        refreshPlayer();
                        Toolkit.getDefaultToolkit().sync();
                        
                    }

                });
                t.start();
            }
        });
    }

    public ArrayList<Lane> getLanes() {
        return lanes;
    }

    private void refreshPlayer() {
        p.setBounds(p.getX(), p.getY() - 1, p.getSIZE(), p.getSIZE());
        p.setBounds(p.getX(), p.getY() + 1, p.getSIZE(), p.getSIZE());
    }

}
