/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.overpass_junction;

import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.imageio.ImageIO;

/**
 *
 * @author csordasadam
 */
public class Props {
    
    ArrayList<Image> goingLeft;
    ArrayList<Image> goingRight;

    public Props()  {
        goingLeft = new ArrayList<>();
        goingRight = new ArrayList<>();
        try {
        loadTextures();
        } catch(IOException e){
            System.out.println(e.getMessage());
        }
    }
    
     private void loadTextures() throws IOException {
        goingLeft.add(ImageIO.read(new File("src/textures/CarsLeft/F1Left.png")));
        goingLeft.add(ImageIO.read(new File("src/textures/CarsLeft/BlueCarLeft.png")));
        goingLeft.add(ImageIO.read(new File("src/textures/CarsLeft/BrownCarLeft.png")));
        goingLeft.add(ImageIO.read(new File("src/textures/CarsLeft/GreenCarLeft.png")));
        goingLeft.add(ImageIO.read(new File("src/textures/CarsLeft/PinkCarLeft.png")));
        goingLeft.add(ImageIO.read(new File("src/textures/CarsLeft/RedCarLeft.png")));
        goingLeft.add(ImageIO.read(new File("src/textures/CarsLeft/YellowCarLeft.png")));

        goingRight.add(ImageIO.read(new File("src/textures/CarsRight/F1Right.png")));
        goingRight.add(ImageIO.read(new File("src/textures/CarsRight/BlueCarRight.png")));
        goingRight.add(ImageIO.read(new File("src/textures/CarsRight/BrownCarRight.png")));
        goingRight.add(ImageIO.read(new File("src/textures/CarsRight/GreenCarRight.png")));
        goingRight.add(ImageIO.read(new File("src/textures/CarsRight/PinkCarRight.png")));
        goingRight.add(ImageIO.read(new File("src/textures/CarsRight/RedCarRight.png")));
        goingRight.add(ImageIO.read(new File("src/textures/CarsRight/YellowCarRight.png")));

    }

    public  ArrayList<Image> getGoingLeft() {
        return goingLeft;
    }

    public  ArrayList<Image> getGoingRight() {
        return goingRight;
    }
}
